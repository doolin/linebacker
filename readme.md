# Linebacker I mission data

B52 Linebacker missions were flown from April until December 1972.

The raw data here is a little messy, but not too bad.

## Development

Use rake tasks create and migrate database.

### Running migrations

Sinatra doesn't the concept of "migrations" built in as Rails does.
There is a gem which will add some of it.

If necessary, `dropdb linebacker` to clean up first.

Now let's run the rake tasks.

* `rake db:migrate` defaults to dev environment.
* `RACK_ENV=test rake db:migrate`
* `RACK_ENV=production rake db:create`
* `RACK_ENV=production rake db:migrate`

Next, run `./linebacker` to provision the database.

The run the server: `rackup config.ru`.


### Exporting the data in CSV

There are rake tasks for exporting CSV data, and I always forgot what
they are, so it's time to write them up.


