$( document ).ready(function() {
  function initMap() {
    var vn = {lat: 18.6665, lng: 105.7166};
    var bounds = new google.maps.LatLngBounds();
    var sw = {lat: 10.7, lng: 104};
    var ne = {lat: 22, lng: 108};
    bounds.extend(sw);
    bounds.extend(ne);
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 9,
      center: vn
    });
    map.fitBounds(bounds);
    return map;
  }

  var map = initMap();
  var markers = [];
  window.map = map;


  function searchDate(date) {
    $.getJSON("/data.json" + "?date=" + date, function(places) {
      $.each(places, function(key, data) {
        var latLng = new google.maps.LatLng(data.lat, data.lng);
        this.marker = new google.maps.Marker({
          position: latLng,
          map: map,
          title: 'whateva'
        });
        markers.push(this.marker);
      });
    });
    console.log(markers);
  }
   // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

  $('form input[type=submit]').click(function(event){
    event.preventDefault();
    clearMarkers()
    var date = $('form select').val()
    searchDate(date)
  })

});
