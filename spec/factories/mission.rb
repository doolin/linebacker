# frozen_string_literal: true
# Want 4 factories:
# 1. Strike from Tapao, Thailand
# 2. Strike from Andersen, Guam
# 3. Interdiction from Tapao, Thailand
# 4. Interdiction from Andersen, Guam

FactoryGirl.define do
  factory :mission_tapao_strike, class: Mission do
    mission_number '20409J8014AA0101S9378SAC'
    date_flown Mission.extract_date_flown('Apr', '09')
    lat 18.6665
    long 105.7166
    aircraft 'B-52D'
    airbase 'TAPAO'
    mission_type 'STRIKE'
    target_type 'refinery'
    resolution ''
  end

  factory :mission_tapao_interdiction, class: Mission do
    mission_number '20409J8018AA0301S9348SAC'
    date_flown Mission.extract_date_flown('Apr', '09')
    lat 16.6333
    long 107.0999
    aircraft 'B-52D'
    airbase 'TAPAO'
    mission_type 'INTERDICTION'
    target_type 'troop'
    resolution ''
  end

  factory :mission_andersen_strike, class: Mission do
    mission_number '20503J8025AA0101S9598SAC'
    date_flown Mission.extract_date_flown('May', '03')
    lat 10.6336
    long 104.5001
    aircraft 'B-52G'
    airbase 'ANDERSEN'
    mission_type 'STRIKE'
    target_type 'area'
    resolution ''
  end

  factory :mission_andersen_interdiction, class: Mission do
    mission_number '20410J8001AA0301S9348SAC'
    date_flown Mission.extract_date_flown('Apr', '10')
    lat 11.6831
    long 106.6333
    aircraft 'B-52D'
    airbase 'ANDERSEN'
    mission_type 'INTERDICTION'
    target_type 'area'
    resolution ''
  end
end
