# frozen_string_literal: true

require 'database_cleaner'
require 'factory_girl'

ENV['RACK_ENV'] = 'test'
require_relative '../config/db'
require_relative '../lib/linebacker/mission'
require_relative '../lib/linebacker/raw_mission'

def raw_mission_fixture_name
  './spec/fixtures/raw_mission_fixture.csv'
end

RSpec.configure do |config|
  DatabaseCleaner.strategy = :transaction
  DatabaseCleaner.clean_with :truncation

  config.include FactoryGirl::Syntax::Methods

  config.before(:suite) do
    FactoryGirl.find_definitions
  end
end
