# frozen_string_literal: true
require_relative './spec_helper'

RSpec.describe Mission do
  subject(:mission) { Mission.new }

  it 'makes connection, validates' do
    expect(mission.valid?).to be true
  end

  describe '.process' do
    xit 'does not build mission with empty attrs' do
    end
  end

  describe '.build_attributes' do
    context 'tapao' do
      context 'strike' do
        it 'builds correct attributes' do
          raw_mission = create :raw_mission_tapao_strike
          expected = attributes_for :mission_tapao_strike
          actual = Mission.build_attributes(raw_mission)
          expect(actual.empty?).to be false
          expect(actual).to eq expected
        end
      end

      context 'interdiction' do
        it 'builds correct attributes' do
          raw_mission = create :raw_mission_tapao_interdiction
          expected = attributes_for :mission_tapao_interdiction
          actual = Mission.build_attributes(raw_mission)
          expect(actual.empty?).to be false
          expect(actual).to eq expected
        end
      end
    end

    context 'andersen' do
      context 'strike' do
        it 'builds correct attributes' do
          raw_mission = create :raw_mission_andersen_strike
          expected = attributes_for :mission_andersen_strike
          actual = Mission.build_attributes(raw_mission)
          expect(actual.empty?).to be false
          expect(actual).to eq expected
        end
      end

      context 'interdiction' do
        it 'builds correct attributes' do
          raw_mission = create :raw_mission_andersen_interdiction
          expected = attributes_for :mission_andersen_interdiction
          actual = Mission.build_attributes(raw_mission)
          expect(actual.empty?).to be false
          expect(actual).to eq expected
        end
      end
    end

    it 'returns {} when incorrect attributes'
  end

  describe '.extract_date_flown' do
    it 'combines f2 and f3 to get date' do
      puts Mission.extract_date_flown('APR', '09')
    end
  end

  describe '.extract_lat_long' do
    let(:field_value) { '000116831N1066333EJBT910AIR INTERD' }

    it 'extracts the lat and long from a field' do
      expect(Mission.extract_lat_long(field_value)).to eq([11.6831, 106.6333])
    end

    xit '' do
      _fv = '73211000166333N1070999EJBT910AIR INTERD'
    end
  end

  describe '.extract_mission_type' do
    context 'strike mission' do
      it 'extracts the mission_type from Tapao' do
        raw_mission = create :raw_mission_tapao_strike
        expect(Mission.extract_mission_type(raw_mission.f4)).to eq 'STRIKE'
      end

      it 'extracts the mission_type from Andersen' do
        raw_mission = create :raw_mission_andersen_strike
        expect(Mission.extract_mission_type(raw_mission.f4)).to eq 'STRIKE'
      end
    end

    context 'interdiction mission' do
      it 'extracts the mission_type from Tapao' do
        raw_mission = create :raw_mission_tapao_interdiction
        expect(Mission.extract_mission_type(raw_mission.f4)).to eq 'INTERDICTION'
      end

      it 'extracts the mission_type from Andersen' do
        raw_mission = create :raw_mission_andersen_interdiction
        expect(Mission.extract_mission_type(raw_mission.f4)).to eq 'INTERDICTION'
      end
    end
  end

  describe '.extract_airbase' do
    context 'strike mission' do
      it 'extracts the airbase from Tapao' do
        raw_mission = create :raw_mission_tapao_strike
        expect(Mission.extract_airbase(raw_mission.f13)).to eq 'TAPAO'
      end

      it 'extracts the aircraft from Andersen' do
        raw_mission = create :raw_mission_andersen_strike
        expect(Mission.extract_airbase(raw_mission.f13)).to eq 'ANDERSEN'
      end
    end

    context 'interdiction mission' do
      it 'extracts the aircraft from Tapao' do
        raw_mission = create :raw_mission_tapao_interdiction
        expect(Mission.extract_airbase(raw_mission.f13)).to eq 'TAPAO'
      end

      it 'extracts the aircraft from Andersen' do
        raw_mission = create :raw_mission_andersen_interdiction
        expect(Mission.extract_airbase(raw_mission.f13)).to eq 'ANDERSEN'
      end
    end
  end

  describe '.extract_target_type' do
    target_types = {
      'empty' => [''],
      'uncatalogued' => ['ARS', 'j. random text'],
      'transhipment' => ['ATRTRANS'],
      'bivouac'      => ['D    ABVBIVOU'],
      'road'         => ['D    ARDROAD'],
      'camp'         => ['D    AECCAMP', 'AECCAMP'],
      'airfield'     => ['D    AAFAIRFL', 'AAFAIRFL'],
      'truck'        => ['D    ATPTRUCK'],
      'base'         => ['ABSBASE', 'D    ABSBASE'],
      'aa site'      => ['D    AAAAAA S', 'AAAAAA S'],
      'building'     => ['D    ABLBLDG', 'ABLBLDG'],
      'troop'        => ['D    ATSTROOP', 'ATSTROOP', 'D    PTSTROOP', 'PTSTROOP',
                         'D   CATSTROOP'],
      'unknown'      => ['PUNUNK/U', 'D    PUNUNK/U', 'AUNUNK/U', 'AUNUNK/U'],
      'area'         => ["PARAREA", "D    PARAREA", "D    AARAREA", "AARAREA",
                         "D   CAARAREA", "CAARAREA"],
      'refinery'     => ['D    ARFREFNR']
    }

    target_types.each do |type, raw|
      context "#{type}" do
        raw.each do |value|
          it "extracts '#{type}' from '#{value}'" do
            expect(Mission.extract_target_type(value)).to eq type
          end
        end
      end
    end
  end

  describe '.extract_aircraft' do
    let(:expected) { 'B-52D' }

    context 'strike mission' do
      it 'extracts the aircraft from Tapao' do
        rm = create :raw_mission_tapao_strike
        expect(Mission.extract_aircraft(rm.f12, rm.f13)).to eq expected
      end

      it 'extracts the aircraft from Andersen' do
        rm = create :raw_mission_andersen_strike
        expect(Mission.extract_aircraft(rm.f12, rm.f13)).to eq 'B-52G'
      end
    end

    context 'interdiction mission' do
      it 'extracts the aircraft from Tapao' do
        rm = create :raw_mission_tapao_interdiction
        expect(Mission.extract_aircraft(rm.f12, rm.f13)).to eq expected
      end

      it 'extracts the aircraft from Andersen' do
        rm = create :raw_mission_andersen_interdiction
        expect(Mission.extract_aircraft(rm.f12, rm.f13)).to eq expected
      end

      context 'B-52G' do
        it 'extracts B-52G' do
          rm = build :raw_mission_b52_g
          expect(Mission.extract_aircraft(rm.f12, rm.f13)).to eq 'B-52G'
        end
      end
    end
  end

  describe 'extract_resolution' do
    resolutions = {
      'aircraft systems'       => 'ASACFT SYS',
      'other'                  => 'OTOTHER',
      'ordnance'               => 'ODORD DEL S',
      'wxwx'                   => 'WXWX',
      'unknown'                => 'UNUNK',
      'engine'                 => 'ENENGINE',
      'electrical malfunction' => 'ELELEC',
      'geen'                   => 'GEEN',
      'tuunk'                  => 'ACCDTUUNK',
      'control malfunction'    => 'CMCTRL',
      'comm failure'           => 'CFCOMM',
      'air accident'           => 'ACAIR',
      'aord'                   => 'OAORD',
      'weapon system'          => 'WSWPNS'
    }

    resolutions.each do |processed, raw|
      it "resolves '#{processed}' from '#{raw}'" do
        expect(Mission.extract_resolution(raw)).to eq processed
      end
    end
  end
end
