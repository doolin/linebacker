# frozen_string_literal: true
require_relative './spec_helper'

RSpec.describe RawMission do
  it { expect(RawMission.new).not_to be nil }

  # TODO: figure out why FactoryGirl adds another row
  # to this count. Maybe want to truncate after this spec
  # runs.
  xit 'processes the fixture csv for raw mission data' do
    RawMission.parse_csv raw_mission_fixture_name
    expect(RawMission.count).to eq 48
  end
end
