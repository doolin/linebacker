# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170401155231) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "missions", id: :serial, force: :cascade do |t|
    t.text "mission_number"
    t.datetime "date_flown"
    t.float "lat"
    t.float "long"
    t.string "aircraft"
    t.string "airbase"
    t.string "mission_type"
    t.string "target_type"
    t.string "resolution"
  end

  create_table "raw_missions", id: :serial, force: :cascade do |t|
    t.string "f1"
    t.string "f2"
    t.string "f3"
    t.string "f4"
    t.string "f5"
    t.string "f6"
    t.string "f7"
    t.string "f8"
    t.string "f9"
    t.string "f10"
    t.string "f11"
    t.string "f12"
    t.string "f13"
    t.string "f14"
    t.string "f15"
    t.string "f16"
    t.string "f17"
    t.string "f18"
    t.string "f19"
    t.string "f20"
    t.string "f21"
  end

end
