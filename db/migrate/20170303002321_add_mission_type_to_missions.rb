class AddMissionTypeToMissions < ActiveRecord::Migration[5.0]
  def change
    add_column :missions, :mission_type, :string
  end
end
