# frozen_string_literal: true
class Missions < ActiveRecord::Migration[5.0]
  def self.up
    create_table :missions do |t|
      t.text :mission_number
      t.timestamp :date_flown
      t.float :lat
      t.float :long
    end
  end

  def self.down
    drop_table :missions
  end
end
