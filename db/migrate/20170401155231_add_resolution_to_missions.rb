class AddResolutionToMissions < ActiveRecord::Migration[5.0]
  def change
    add_column :missions, :resolution, :string
  end
end
