class AddTargetTypeToMission < ActiveRecord::Migration[5.0]
  def change
    add_column :missions, :target_type, :string
  end
end
