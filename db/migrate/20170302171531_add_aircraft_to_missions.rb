class AddAircraftToMissions < ActiveRecord::Migration[5.0]
  def change
    add_column :missions, :aircraft, :string
  end
end
