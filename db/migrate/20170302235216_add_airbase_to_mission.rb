class AddAirbaseToMission < ActiveRecord::Migration[5.0]
  def change
    add_column :missions, :airbase, :string
  end
end
