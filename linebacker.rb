#!/usr/bin/env ruby
# frozen_string_literal: true

require 'active_record'

ENV['RACK_ENV'] = 'production'
require_relative './config/db'

require_relative './db/migrate/001_raw_missions'
ActiveRecord::Migrator.migrate(RawMissions.up) unless RawMissions.data_source_exists? :raw_missions
require_relative './db/migrate/002_missions'
ActiveRecord::Migrator.migrate(Missions.up) unless Missions.data_source_exists? :missions

def fulldata
  # TODO: move this to config.yml
  '/Users/doolin/Documents/personal/consim/linebacker1/NorthVietnamB52strikes/with_headers.csv'
end

def demodata
  'data/first_200_with_headers.csv'
end

demo = false
datafile = demo ? demodata : fulldata

require_relative './lib/linebacker/raw_mission'
RawMission.destroy_all
RawMission.parse_csv datafile
puts "RawMission: #{RawMission.count} rows"

require_relative './lib/linebacker/mission'
Mission.destroy_all
Mission.process
puts "Mission: #{Mission.count} rows"
