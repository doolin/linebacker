# frozen_string_literal: true
require 'active_record'
require 'sinatra/activerecord'
require 'sinatra/activerecord/rake'
require 'rubocop/rake_task'
require 'ap'
require 'aws-sdk'

task default: [:clean]

desc 'clean up db files'
task :clean do
  `rm -rf *.db *.sqlite3`
end

RuboCop::RakeTask.new(:rubocop) do |t|
  t.options = ['--display-cop-names']
end

# Question: is it possible to default a task within a namespace?
# http://stackoverflow.com/questions/1579231/default-task-for-namespace-in-rake
namespace :export do
  namespace :csv do
    desc 'archive to S3'
    task :archive do
      credentials = Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'])
      s3 = Aws::S3::Resource.new(
        region: ENV['AWS_REGION_LINEBACKER'],
        credentials: credentials
      )
      obj = s3.bucket('consim').object('linebacker1/missions.csv')
      obj.upload_file('/tmp/missions.csv')
    end

    desc 'export from postgres'
    task :postgres do
      `psql -d linebacker -c "COPY missions TO STDOUT WITH CSV HEADER" > /tmp/missions.csv`
    end

    desc 'export from sqlite'
    task :sqlite do
      `sqlite3 -header -csv lb1.db "select mission_number,date_flown,lat,long,aircraft from missions;" > /tmp/missions.csv`
    end

    desc 'move csv output to local archive'
    task :local_copy do
      archive_location = '/Users/doolin/Documents/personal/consim/linebacker1/NorthVietnamB52strikes/'
      `mv /tmp/missions.csv #{archive_location}`
    end
  end
end

desc 'show the map'
task :map do
  `open maps_demo.html`
end

require 'haml_lint/rake_task'
desc 'lint the haml'
HamlLint::RakeTask.new do |t|
  t.files = ['views']
  t.quiet = false
end
