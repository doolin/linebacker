# frozen_string_literal: true
require 'sinatra/base'
require 'sinatra/activerecord'

require_relative './lib/linebacker/mission'

class Linebacker < Sinatra::Base
  set :bind, '0.0.0.0'
  set :port, 4569

  ENV['RACK_ENV'] = 'production'
  require_relative './config/db'

  get '/' do
    @mission = Mission.last
    @dates = (Date.new(1972, 4, 9)..Date.new(1972, 12, 31))
    haml :index
  end

  get '/data.json' do
    date = params[:date] || '1972-04-09'
    Mission.where(date_flown: date).map do |mission|
      { lat: mission.lat, lng: mission.long, title: '' }
    end.to_json
  end
end
