# frozen_string_literal: true
require 'active_record'
require 'ap'
require 'csv'
require 'pry'

# RawMissions are the row-wise data taken directly from a CSV dump
# of the mission spreadsheet.
class RawMission < ActiveRecord::Base
  def self.parse_csv(datafile)
    CSV.foreach(datafile, headers: true) do |row|
      next if row[14].nil?
      next if row[14][0] == 'S'
      next if row[3].match?(/REFUEL/)
      RawMission.create!(row.to_hash)
    end
  end
end
