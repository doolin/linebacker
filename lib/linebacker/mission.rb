# frozen_string_literal: true
require 'active_record'
require 'ap'
require 'csv'
require 'pry'

# Each Mission is created by processing a RawMission to combine
# and split various RawMission fields into sensible mission data.
class Mission < ActiveRecord::Base
  def self.build_attributes(raw_mission)
    lat, long = extract_lat_long raw_mission.f11
    aircraft = extract_aircraft raw_mission.f12, raw_mission.f13
    return {} if lat > 30
    {
      mission_number: raw_mission.f1,
      date_flown: extract_date_flown(raw_mission.f2, raw_mission.f3),
      lat: lat,
      long: long,
      aircraft: aircraft,
      airbase: extract_airbase(raw_mission.f13),
      mission_type: extract_mission_type(raw_mission.f4),
      target_type: extract_target_type(raw_mission.f9),
      resolution: extract_resolution(raw_mission.f17)
    }
  end

  # Some of this information may be in Table 26.
  # linebacker=# select f17, count(f17) from raw_missions where f17 is not null
  #  group by f17 order by count(f17) desc;
  #        f17       | count
  # -----------------+-------
  #  ASACFT SYS      |   258
  #  OTOTHER         |   246
  #  ODORD DEL S     |    57
  #  WXWX            |    31
  #  UNUNK           |    29
  #  ENENGINE        |    18
  #  ELELEC MALF     |    10
  #  GEEN GND AC     |     6
  #  ACAIR ACCDTUUNK |     1
  #  CMCTRL MALF     |     1
  #  CFCOMM FAIL     |     1
  #  ACAIR ACCDT     |     1
  #  OAORD AIM S     |     1
  #  WSWPNS SYS      |     1
  def self.extract_resolution(field)
    case field
    when /ASACFT/ then
      'aircraft systems'
    when /OTOTHER/ then
      'other'
    when /ODORD/ then
      'ordnance'
    when /WXWX/ then
      'wxwx'
    when /UNUNK/ then
      'unknown'
    when /ENENGINE/ then
      'engine'
    when /ELELEC/ then
      'electrical malfunction'
    when /GEEN/ then
      'geen'
    when /ACCDTUUNK/ then
      'tuunk'
    when /CMCTRL/ then
      'control malfunction'
    when /CFCOMM/ then
      'comm failure'
    when /ACAIR/ then
      'air accident'
    when /OAORD/ then
      'aord'
    when /WSWPNS/ then
      'weapon system'
    else
      ''
    end
  end

  def self.extract_date_flown(month, day)
    Date.parse("#{month} #{day} 1972")
  end

  # This is not testable as written. The RawMission needs to be
  # pulled out of here and process needs to be parameterized.
  # Or, make a MissionProcessor or MissionBuilder class which
  # takes care of all the provisioning of the Missions table.
  def self.process
    RawMission.all.each do |raw_mission|
      attrs = build_attributes(raw_mission)
      Mission.create!(attrs) unless attrs.empty?
    end
  end

  # TODO: add YARD
  #
  #  linebacker=# select f6,f7,f8,f9,f10 from raw_missions limit 1;
  #             f6            | f7  |  f8  |      f9       |         f10
  # --------------------------+-----+------+---------------+---------------------
  #  AL716               USAF | SAC | 1401 | D    ARFREFNR | Y       VNN VIET0
  #  AL222               USAF | SAC | 1201 | D    AAAAAA S | ITE UND VSS VIET102
  #
  # We're interested in the target type, but there seems to be a lot of
  # cruft associated with it. Let's dismantle this result character by character.
  # The field we want is 'f9'. From the documentation we have the following:
  #
  # TCODE  143-146, 4 chars,
  # TGTSUB 147-147, 1 char
  # TTYPE  148-149, 2 chars
  # DTTYPE 150-162, 13 chars
  #
  # From the reference defined in Table 6, we find that the two letters
  # 'RF' denote "refinery," allowing the working assumption that we have
  # TTYPE starting at character 148. This is followed by 'REFNR' in 150-154.
  # But carrying over we have 'Y       ', which is 13 characters. Whether
  # the characters following start at position 163 (CTYHIT) is unknown.
  #
  # Given the assumption of TTYPE and character 148, the previous character
  # could be the 1 character TGTSUB, this case 'A'. Preceding is TCODE, which
  # is 4 characters long and may be represented by the 4 empty characters
  # following the 'D' at the beginning of f9.

  def self.extract_target_type(field)
    case field
    when /AREA/ then
      'area'
    when /UNUNK/ then
      'unknown'
    when /TROOP/ then
      'troop'
    when /BLBLDG/ then
      'building'
    when /AAAAA/ then
      'aa site'
    when /BSBASE/ then
      'base'
    when /D    ATPTRUCK/ then
      'truck'
    when /AFAIRFL/ then
      'airfield'
    when /ECCAMP/ then
      'camp'
    when /RDROAD/ then
      'road'
    when /BVBIVOU/ then
      'bivouac'
    when /TRTRANS/ then
      'transhipment'
    when /D    ARFREFNR/ then
      'refinery'
    when /ARS/ then
      'uncatalogued'
    when /.+/ then
      'uncatalogued'
    else
      'empty'
    end
  end

  # TODO: add YARD
  def self.extract_mission_type(field)
    return 'INTERDICTION' if field.match(/INTERDICT/)
    return 'STRIKE' if field.match(/STRIKE/)
    ''
  end

  # TODO: make this into real rubydoc using YARD
  def self.extract_lat_long(field)
    raw = field.slice(1..-1)
               .split('E')
               .map do |loc|
            loc
              .split 'N'
          end[0]
               .map(&:to_f)
    lat = raw[0] / 10_000.0
    long = raw[1] / 10_000.0
    [lat, long]
  end

  # TODO: make this into real rubydoc using YARD
  # We want to match either 'B-52' or 'ICT  B-52' and
  # return only the 'B-52'
  def self.extract_aircraft(f12, f13)
    aircraft = f12.match(/(ICT\s+)*(B-52)/)[2]
    model = f13.match(/(G|D){1}.*/)[1]
    aircraft << model
  end

  # # TODO: add YARD docs
  # 'D003009500 U TAPAO   23502350'
  # 'D003030600 ANDERSEN  01160116'
  def self.extract_airbase(field)
    return 'TAPAO' if field.match(/TAPAO/)
    return 'ANDERSEN' if field.match(/ANDERSEN/)
    ''
  end
end
